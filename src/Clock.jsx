import React,{Component} from 'react';

class Clock extends React.Component {
    constructor(props) {
      super(props);

      //ini state
      this.state = { date: new Date() };
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.updateClock(), 1000);
      }
      componentWillUnmount() {
        clearInterval(this.timerID);
      }

      
      updateClock() {
        this.setState({
          date: new Date()
         } ) }
    
  
    render() {
      return (
        <div>
    
          <h2>Jam {this.state.date.toLocaleTimeString()}.</h2>
        </div>
      );
    }
}
export default Clock;