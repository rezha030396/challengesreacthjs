import React from 'react';
import Avatar from "./Avatar"
import UserName from "./UserName"
import Bio from "./Bio"


export default function UserProfile() {
    
    return (
            <div>
            <Avatar />
            <UserName Name ="Rezha Maulana Jaya" />
            <Bio />
            </div>
                    );
  }