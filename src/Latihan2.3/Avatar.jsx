import React from 'react';
import Foto from './Foto.jpg';
import './Style.css'


function Avatar(){
    return(
        <div>
            <img src={Foto} alt="Foto"/>
        </div>
    )
}

export default Avatar